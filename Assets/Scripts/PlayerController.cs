using UnityEngine;


public class PlayerController : MonoBehaviour
{
  private const string ANIMATOR_SPEED = "Speed";
  public float movement_speed = 0.5f;
  public float rotation_speed = 2f;
  private Animator anim;
  private Rigidbody rb;

  // Start called once for object (after Awake)
  // To learn more https://docs.unity3d.com/Manual/ExecutionOrder.html
  private void Start()
  {
    anim = GetComponent<Animator>(); // Geting component of type "Animator" to use it later
    rb = GetComponent<Rigidbody>(); // Geting component of type "RigidBody" to use it later
  }

  // FixedUpdate called once per fixed time. check Edit->ProjectSettings->Time
  private void FixedUpdate()
  {
    // getting controller input value for axises
    float h = Input.GetAxis( "Horizontal" );
    float v = Input.GetAxis( "Vertical" );

    anim.SetFloat( ANIMATOR_SPEED, v ); // setting animator parameter
    //anim.GetFloat( ANIMATOR_SPEED ); // to get float param. use anim.Get... to get parameters of other types

    Vector3 translation = transform.forward * v * movement_speed; // define movement direction and value

    rb.MovePosition( transform.position + translation ); // move character useing rigid body (to get physics to work)
    transform.Rotate( Vector3.up, h * rotation_speed ); // rotate character around his Y axis
    
  }
}
