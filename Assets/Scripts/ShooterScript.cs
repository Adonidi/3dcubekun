using UnityEngine;


public class ShooterScript : MonoBehaviour
{
  public GameObject bullet_prototype = null;
  public GameObject bullet_pivot = null;
  
  // The function for animation event
  private void Fire()
  {
    if ( !bullet_prototype || !bullet_pivot )
      return;
    GameObject go = Instantiate( bullet_prototype, bullet_pivot.transform.position, bullet_pivot.transform.rotation );
  }
}
