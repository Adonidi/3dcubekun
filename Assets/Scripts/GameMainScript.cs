using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameMainScript : MonoBehaviour
{

  public static int enemies_killed { get; private set; }

  public Text enemies_text = null;

  private static GameMainScript inst = null;

	// Use this for initialization
	void Start ()
	{
	  inst = this;
    enemies_killed = 0;
  }

  public static void EnemyKilled()
  {
    ++enemies_killed;
    if( inst != null )
      inst.UpdateUI();
  }

  private void UpdateUI()
  {
    if ( enemies_text )
      enemies_text.text = "Enemies: " + enemies_killed;
  }
}
